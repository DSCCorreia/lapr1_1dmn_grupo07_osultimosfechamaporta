/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teste;

import org.la4j.Matrix;
import org.la4j.matrix.DenseMatrix;
import org.la4j.matrix.dense.Basic2DMatrix;
import org.la4j.decomposition.EigenDecompositor;

/**
 *
 * @author Daniel Correia
 */
public class TESTE {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        int mat[][] = {{0, 1, 0}, {1, 0, 1}, {0, 1, 0}};

        for (int i = 0; i < 3; i++) {
            System.out.println("");
            for (int j = 0; j < 3; j++) {
                System.out.print(mat[i][j] + " ");
            }
        }
        System.out.println("");
        System.out.println("");
        double[][] matDouble = new double[3][3];

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                matDouble[i][j] = (double) mat[i][j];
            }
        }

        Matrix a = new Basic2DMatrix(matDouble);

        //Obtem valores e vetores próprios fazendo "Eigen Decomposition"
        EigenDecompositor eigenD = new EigenDecompositor(a);

        Matrix[] mattD = eigenD.decompose();

        for (int i = 0; i < 2; i++) {

            System.out.println(mattD[i]);

        }

        double matA[][] = mattD[0].toDenseMatrix().toArray();

        double matB[][] = mattD[1].toDenseMatrix().toArray();

        int indiceMaiorValor = -1;
        double maiorValor = Double.MIN_VALUE;

        for (int i = 0; i < matB.length; i++) {
            if (matB[i][i] > maiorValor) {
                maiorValor = matB[i][i];
                indiceMaiorValor = i;
            }
        }
        
        System.out.println(maiorValor);
        System.out.println(indiceMaiorValor);

    }

}
