/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr1_project;

import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URI;
import java.util.Date;
import java.text.DateFormat;
import java.util.Scanner;
import java.util.Formatter;
import java.text.DecimalFormat;
import org.la4j.Matrix;
import org.la4j.matrix.dense.Basic2DMatrix;
import org.la4j.decomposition.EigenDecompositor;

/**
 *
 * @author OsÚltimosFechamAPorta Grupo07
 */
public class LAPR1_PROJECT_MAIN {

    private static final Scanner sc = new Scanner(System.in);
    private static final String SEPARADOR_DADOS_FICHEIRO = ",";
    private static final int N_CAMPOS_INFO = 5;
    private static final int ENTIDADES = 200;
    private static final int OUTPUT = 2000;
    private static final String SEPARADOR_NOME_FICH = "_";
    private static final String SEPARADOR_EXTENSAO_FICH = ".txt";
    private static final String SEPARADOR_ORIENTACAO_FICH = ":";
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException {
        DecimalFormat df = new DecimalFormat("0.000"); // formatação para 3 casas decimais
        String[][] entidades = new String[ENTIDADES][N_CAMPOS_INFO]; // guarda as linhas do ficheiro dos nós

        String[] vetorOutput = new String[ENTIDADES]; // guarda cálculos efetuados durante a utilização da aplicação
        int count = 0; // contador para o vetor de output

        int nEntidades = 0;
        int[][] ramos = new int[ENTIDADES][ENTIDADES]; // guarda os ramos do ficheiro
        int op = 0;

        if (args[0].equals("-n") && args.length == 3) {
            nEntidades = lerFicheiroNos(args[1], entidades);
            if (nEntidades != 0) {
                System.out.println("Entidades da Rede Social");
                for (int i = 0; i < nEntidades; i++) {
                    System.out.printf("%n");
                    for (int j = 0; j < N_CAMPOS_INFO; j++) {
                        System.out.printf("%s; ", entidades[i][j]);
                    }
                }
            } else {
                System.out.println("Ficheiro não encontrado.");
            }

            System.out.println("");

            String orientacao = lerFicheiroRamos(args[2], ramos, nEntidades);
            if (!orientacao.equals("impossivel")) {
                System.out.println("Ficheiro carregado com sucesso");
            } else {
                System.out.println("Ficheiro não executável");

            }

            if (orientacao.equals("nonoriented")) {

                do {
                    op = menuNaoOrientado();
                    switch (op) {

                        case 1:
                            for (int i = 0; i < nEntidades; i++) {
                                int grau = grauNode(entidades[i][0], ramos, nEntidades);
                                String output = "Grau da entidade " + entidades[i][0] + " : " + grau;
                                vetorOutput[count] = output;
                                count++;
                                System.out.println(output);
                            }
                            break;

                        case 2:
                            float grauMedio = grauMedia(ramos, nEntidades);
                            String output = "Grau Medio: " + df.format(grauMedio);

                            vetorOutput[count] = output;
                            count++;
                            System.out.println(output);
                            break;
                        case 3:
                            float densidade = densidade(ramos, nEntidades);

                            output = "Densidade: " + df.format(densidade);

                            vetorOutput[count] = output;
                            count++;
                            System.out.println(output);
                            break;
                        case 4:
                            System.out.println("Centralidade do Vector Próprio");

                            double[] centrality = eigenvectorCentrality(ramos, nEntidades);

                            output = "Valor Próprio: " + centrality[0];

                            output = output + "\nCentralidade: ";

                            for (int i = 1; i < centrality.length; i++) {
                                output = output + centrality[i] + " ";
                            }

                            vetorOutput[count] = output;
                            count++;
                            System.out.println(output);
                            break;

                        case 5:
//                    opcao = 0;
                            System.out.println("Digite o comprimento da ligação que pretente obter");
                            int caminhos = sc.nextInt(); // comprimento; potencia da matriz

                            String fi = "";
                            int[][] aux = ramos;
                            for (int i = 1; i < caminhos; i++) {
                                aux = multiplicaMatriz(ramos, aux, nEntidades);
                                vetorOutput[count] = "Matriz Potência " + (i + 1);
                                count++;
                                System.out.println("");
                                System.out.printf("%nMatriz Potência %d", (i + 1));
                                System.out.println("");
                                for (int z = 0; z < nEntidades; z++) {
                                    System.out.println("");
                                    fi = "";
                                    for (int j = 0; j < nEntidades; j++) {
                                        System.out.printf("%d | ", aux[z][j]);
                                        fi += aux[z][j] + " | ";
                                    }
                                    vetorOutput[count] = fi;
                                    count++;
                                }
                            }
                            break;

                        case 6:
                            String[] nome = args[1].split(SEPARADOR_NOME_FICH); // obtem o nome da rede social, que se encontra no segundo elemento do vetor
                            FicheiroOutput(nome[1], count, vetorOutput);
                            break;
                        case 0:
                            System.out.println("FIM");
                            break;
                        default:
                            System.out.println("Opção incorreta. Repita");
                            break;
                    }

                } while (op != 0);
            } else if (orientacao.equals("oriented")) {
                do {
                    op = menuOrientado();
                    switch (op) {

                        case 1:
                            for (int i = 0; i < nEntidades; i++) {
                                int grauEntrada = LAPR1_PROJ_GrafosOrientados.calcularGrauEntrada(entidades[i][0], ramos, nEntidades);
                                String output = "Grau de Entrada da entidade " + entidades[i][0] + " : " + grauEntrada;
                                vetorOutput[count] = output;
                                count++;
                                System.out.println(output);
                            }
                            break;

                        case 2:
                            for (int i = 0; i < nEntidades; i++) {
                                int grauSaida = LAPR1_PROJ_GrafosOrientados.calcularGrauSaida(entidades[i][0], ramos, nEntidades);
                                String output = "Grau de Saída da entidade " + entidades[i][0] + " : " + grauSaida;
                                vetorOutput[count] = output;
                                count++;
                                System.out.println(output);
                            }
                            break;

                        case 3:
                            String output = "";
                            System.out.println("Digite o damping factor: ");
                            double dampingFactor = sc.nextDouble();
                            if (dampingFactor >= 0 && dampingFactor <= 1) {
                                System.out.println("Digite o número de iterações a realizar: ");
                                int nIteracoes = sc.nextInt();
                                if (nIteracoes > 0) {
                                    System.out.println("PageRank pelo Algoritmo");
                                    vetorOutput[count] = "PageRank pelo Algoritmo com " + nIteracoes;
                                    count++;
                                    double[] pageRankVector = LAPR1_PROJ_GrafosOrientados.calcularPageRank(ramos, nEntidades, nIteracoes, dampingFactor);
                                    pageRankVector = LAPR1_PROJ_GrafosOrientados.vectorNormalization(pageRankVector);
                                    System.out.println("");
                                    for (int i = 0; i < nEntidades; i++) {
                                        output = output + pageRankVector[i] + " ";
                                    }
                                    output = "Vetor Normalizado: " + output;
                                    vetorOutput[count] = output;
                                    count++;
                                    System.out.println(output);

                                    int maiorPageRank = LAPR1_PROJ_GrafosOrientados.maiorPageRank(pageRankVector, nEntidades);

                                    System.out.println("");
                                    output = "Página com maior PageRank(por algoritmo) é: " + entidades[maiorPageRank][0];

                                    vetorOutput[count] = output;
                                    count++;
                                    System.out.println(output);

                                    break;
                                } else {
                                    System.out.println("Valor de Inválido!");
                                    break;
                                }
                            } else {
                                System.out.println("Valor de Damping Factor Inválido!");
                                break;
                            }

                        case 4:
                            System.out.println("Digite o damping factor: ");
                            double dampingFactorCent = sc.nextDouble();
                            if (dampingFactorCent >= 0 && dampingFactorCent <= 1) {
                                output = "PageRank pela Centralidade do Vetor Próprio: ";
                                vetorOutput[count] = output;
                                count++;
                                System.out.println(output);

                                double[] pageRankVector = LAPR1_PROJ_GrafosOrientados.calcularPageRankByCentrality(ramos, nEntidades, dampingFactorCent);
                                pageRankVector = LAPR1_PROJ_GrafosOrientados.vectorNormalization(pageRankVector);

                                for (int i = 0; i < nEntidades; i++) {
                                    output = output + pageRankVector[i] + " ";
                                }
                                output = "Vetor Normalizador(Centralidade): " + output;
                                vetorOutput[count] = output;
                                count++;
                                System.out.println(output);
                                int maiorPageRank = LAPR1_PROJ_GrafosOrientados.maiorPageRank(pageRankVector, nEntidades);
                               
                                output = "Página com maior PageRank(por vetor próprio) é: " + entidades[maiorPageRank][0];

                                vetorOutput[count] = output;
                                count++;
                                System.out.println(output);
                                

                                break;
                            } else {
                                System.out.println("Valor de Damping Factor Inválido!");
                                break;
                            }

                        case 5:
                            
                            System.out.println("Digite o damping factor: ");
                            double dampingFactorB = sc.nextDouble();
                            if (dampingFactorB >= 0 && dampingFactorB <= 1) {
                                System.out.println("Digite o número de iterações a realizar: ");
                                int numIteracoes = sc.nextInt();
                                if (numIteracoes > 0) {
                                    String outputDist = "Distância euclidiana entre vectores PageRank calculados pelo Algoritmo com " + numIteracoes + " e pela Centralidade do Vetor Próprio: ";
                                    double[] pageRankVectorAlgoritmo = LAPR1_PROJ_GrafosOrientados.calcularPageRank(ramos, nEntidades, numIteracoes, dampingFactorB);
                                    pageRankVectorAlgoritmo = LAPR1_PROJ_GrafosOrientados.vectorNormalization(pageRankVectorAlgoritmo);
                                    double[] pageRankVectorCentralidade = LAPR1_PROJ_GrafosOrientados.calcularPageRankByCentrality(ramos, nEntidades, dampingFactorB);
                                    pageRankVectorCentralidade = LAPR1_PROJ_GrafosOrientados.vectorNormalization(pageRankVectorCentralidade);

                                    double distanciaEuclidiana = LAPR1_PROJ_GrafosOrientados.distanciaEuclidiana(pageRankVectorAlgoritmo, pageRankVectorCentralidade);

                                    outputDist = outputDist + distanciaEuclidiana;

                                    vetorOutput[count] = outputDist;
                                    count++;
                                    System.out.println("");
                                    System.out.println(outputDist);

                                    break;
                                } else {
                                    System.out.println("Valor Inválido!");
                                    break;
                                }
                            } else {
                                System.out.println("Valor de Damping Factor Inválido!");
                                break;
                            }

                        case 6:
                            try {
                                System.out.println("Digite o damping factor: ");
                                double dampingFactorP = sc.nextDouble();
                                if (dampingFactorP >= 0 && dampingFactorP <= 1) {
                                    System.out.println("Digite o número de iterações a realizar: ");
                                    int numeroIteracoes = sc.nextInt();
                                    int maiorPageRankPrint = -1;

                                    if (numeroIteracoes > 0) {

                                        double[] pageRankVectorPage = LAPR1_PROJ_GrafosOrientados.calcularPageRank(ramos, nEntidades, numeroIteracoes, dampingFactorP);
                                        pageRankVectorPage = LAPR1_PROJ_GrafosOrientados.vectorNormalization(pageRankVectorPage);

                                        maiorPageRankPrint = LAPR1_PROJ_GrafosOrientados.maiorPageRank(pageRankVectorPage, nEntidades);

                                        Desktop desktop = java.awt.Desktop.getDesktop();
                                        URI oURL = new URI(entidades[maiorPageRankPrint][4]);
                                        desktop.browse(oURL);

                                    } else {
                                        System.out.println("Valor Inválido!");
                                        break;
                                    }
                                } else {
                                    System.out.println("Valor de Damping Factor Inválido!");
                                    break;
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        case 7:
                            String[] nome = args[1].split(SEPARADOR_NOME_FICH); // obtem o nome da rede social, que se encontra no segundo elemento do vetor
                            FicheiroOutput(nome[1], count, vetorOutput);
                            break;

                        case 0:
                            System.out.println("FIM");
                            break;
                        default:
                            System.out.println("Opção incorreta. Repita");
                            break;
                    }

                } while (op != 0);
            }

        } else {
            if (args[0].equals("-t") && args.length == 7) {

                if (args[1].equals("-k")) {

                    nEntidades = lerFicheiroNos(args[5], entidades);
                    if (nEntidades != 0) {
                        System.out.println("Entidades da Rede Social");
                        for (int i = 0; i < nEntidades; i++) {
                            System.out.printf("%n");
                            for (int j = 0; j < N_CAMPOS_INFO; j++) {
                                System.out.printf("%s; ", entidades[i][j]);
                            }
                        }
                    } else {
                        System.out.println("Ficheiro não encontrado.");
                    }

                    System.out.println("");

                    String orientacao = lerFicheiroRamos(args[6], ramos, nEntidades);
                    if (!orientacao.equals("impossivel")) {
                        System.out.println("Ficheiro carregado com sucesso");
                    } else {
                        System.out.println("Ficheiro não executável");

                    }
                    if (orientacao.equals("nonoriented")) {
                        int potencia = Integer.parseInt(args[2]);

                        try {
                            executarTudoNaoOrientado(args[5], args[6], potencia, entidades, vetorOutput, count, nEntidades, ramos);
                        } catch (FileNotFoundException fne) {
                            System.out.println("Ficheiro não encontrado.");
                            System.exit(0);
                        }
                    } else if (orientacao.equals("oriented")) {
                        int nOrientacoes = Integer.parseInt(args[2]);
                        double dampingFactor = Double.parseDouble(args[4]);
                        executarTudoOrientado(args[5], args[6], dampingFactor, nOrientacoes, entidades, vetorOutput, count, nEntidades, ramos);
                    }
                } else {
                    System.out.println("Segundo argumento inválido.");
                    System.exit(0);
                }
            } else {
                System.out.println("Erro nos argumentos.");
                System.exit(0);
            }
        }
    }

    private static int menuNaoOrientado() {
        String texto = "\nMENU - Ligações não orientadas:"
                + "\n Calcular Grau - 1"
                + "\n Calcular Grau Médio - 2"
                + "\n Calcular densidade - 3"
                + "\n Calcular centralidade de vetor próprio - 4 "
                + "\n Calcular potência de uma matriz - 5"
                + "\n Criação Ficheiro com calculos efetuados com a aplicação - 6"
                + "\n Terminar - 0"
                + "\n Qual a sua opção ?";

        System.out.println(texto);

        int op = sc.nextInt();
        return op;
    }

    private static int menuOrientado() {
        String texto = "\nMENU - Ligações orientadas:"
                + "\n Calcular Grau Entrada - 1"
                + "\n Calcular Grau Saída - 2"
                + "\n Calcular PageRank pelo Algoritmo - 3"
                + "\n Calcular PageRank pela Centralidade do Vetor Próprio - 4"
                + "\n Calcular Distâncias Euclidiana - 5"
                + "\n Abrir página com maior PageRank - 6"
                + "\n Criação Ficheiro com calculos efetuados com a aplicação - 7"
                + "\n Terminar - 0"
                + "\n Qual a sua opção ?";

        System.out.println(texto);

        int op = sc.nextInt();
        return op;
    }

    /**
     * Dado que seriam facultados ficheiros de nós em formato .csv, foi
     * necessário criar uma função que guarde os elementos lidos do ficheiro.
     * Recebe por parâmetro o nome do ficheiro e uma matriz de nós, retorna o
     * número de elementos inseridos.
     *
     * @param nomeFich nome do ficheiro para ler
     * @param nos matriz de nós
     * @return número de elementos inseridos
     * @throws FileNotFoundException
     */
    private static int lerFicheiroNos(String nomeFich, String[][] nos) throws FileNotFoundException {
        int nElementos = 0;
        Scanner fInput = new Scanner(new File(nomeFich));
        fInput.nextLine(); // passa a linha do cabeçalho a frente
        while (fInput.hasNext()) {
            String linha = fInput.nextLine();
            if (!linha.isEmpty()) {
                String[] linhaRamos = linha.trim().split(SEPARADOR_DADOS_FICHEIRO); // coloca dados da linha separado por ; num vetor
                nElementos = guardarMatrizNos(linhaRamos, nos, nElementos); // recebemos número de elementos
            }
        }
        fInput.close();
        return nElementos;
    }

    /**
     * A função guardarMatrizNos guarda os elementos na matriz, recebe por
     * parâmetros linha lida do ficheiro de entrada, a matriz de nós e o número
     * de elementos. Retorna o número atualizado de elementos da matriz.
     *
     * @param linhaRamos linha lida do ficheiro de entrada
     * @param nos matriz de nós
     * @param nElementos número de elementos
     * @return número atualizado de elementos da matriz
     */
    private static int guardarMatrizNos(String[] linhaRamos, String[][] nos, int nElementos) {

        if (linhaRamos.length == N_CAMPOS_INFO) {
            String cod = linhaRamos[0].trim();
            int pos = pesquisarElemento(cod, nElementos, nos); // procura id na matriz entidades
            if (pos == -1) { // verifica se já existe algum id introduzido
                for (int i = 0; i < N_CAMPOS_INFO; i++) {
                    nos[nElementos][i] = linhaRamos[i].trim(); // guarda linha do ficheiro numa nova linha da matriz
                }
                nElementos++; // incremento número de elementos
            }
            return nElementos;
        } else {
            System.out.println("Ficheiro com dados insuficientes");
            return -1;
        }
    }

    /**
     * O método de pesquisarElemento vai verificar se o elemento já existe na
     * matriz, vai passar por parâmetro o valor a pesquisar, o número de
     * elementos e a matriz a pesquisar. Retorna a linha onde se encontra o
     * elemento, ou se não estiver presente, retorna -1.
     *
     * @param valor valor a pesquisar
     * @param nE número de elementos
     * @param mat matriz a pesquisa
     * @return linha onde se encontra o elemento
     */
    private static int pesquisarElemento(String valor, int nE, String[][] mat) {
        for (int i = 0; i < nE; i++) {
            if (mat[i][0].equals(valor)) { // pesquisa pelo id
                return i;
            }
        }
        return -1;
    }

    /**
     * O segundo ficheiro de entrada, tem formato .csv, e descreve os ramos. Foi
     * criada uma função para ler os ficheiros de ramos, que recebe por
     * parâmetro o nome do ficheiro, a matriz de ramos e o número de elementos.
     * A função retorna um boolean, falso se o ficheiro é inválido e verdadeiro
     * se leu corretamente o ficheiro.
     *
     * @param nomeFich nome do ficheiro a ler
     * @param ramos matriz de ramos
     * @param nElementos número de elementos
     * @return se o ficheiro é válido ou inválido
     * @throws FileNotFoundException
     */
    private static String lerFicheiroRamos(String nomeFich, int[][] ramos, int nElementos) throws FileNotFoundException {
        Scanner fInput = new Scanner(new File(nomeFich));
        int i = 0;
        String orientacao = fInput.nextLine();
        String[] orientation = orientacao.split(SEPARADOR_ORIENTACAO_FICH);
        fInput.nextLine(); // ignora a linha do cabeçalho
        int nTotal = 0;
        int aux = 0;
        while (fInput.hasNext()) {
            String linha = fInput.nextLine();
            String[] linhaRamos = linha.trim().split(SEPARADOR_DADOS_FICHEIRO); // guarda linha do ficheiro num vetor separado por ;
            if (!linha.isEmpty()) {
                aux = guardarMatrizAdjacencias(linhaRamos, ramos, orientation[1]);
                if (aux == -1) { // caso exista uma ligação entre o mesmo nó; ex : s01; s01; 1
                    System.out.println("Ficheiro inválida, introduza um novo ficheiro");
                    return "impossivel";
                }

            }
        }

        fInput.close();
        return orientation[1];
    }

    /**
     * A função guardarMatrizAdjacencias vai preencher a matriz de adjacências,
     * recebendo por parâmetro a linha lida do ficheiro de ramos e a matriz de
     * ramos. Verificando-se um self-loop a função retorna -1, retorna 0 caso a
     * ligação já exista e retorna 1 caso a ligação seja preenchida.
     *
     * @param linhaRamos linha lida do ficheiro de ramos
     * @param ramos matriz de ramos
     * @return -1 se é self-loop, 0 se já existe a ligação, ou 1 se é uma
     * ligação nova
     */
    private static int guardarMatrizAdjacencias(String[] linhaRamos, int[][] ramos, String orientacao) {
        String from = linhaRamos[0];
        String too = linhaRamos[1];
        int from1 = 0;
        int too1 = 0;

        from1 = Integer.parseInt(from.substring(1)) - 1; // retira a parte numerica de sXX, transforma num inteiro e subtrair 1 para igualar a posição

        too1 = Integer.parseInt(too.substring(1)) - 1;  // retira a parte numerica de sXX, transforma num inteiro e subtrair 1 para igualar a posição
        if (orientacao.equals("nonoriented")) {
            if (from1 == too1) {
            System.out.println("Duas entidades não se podem ligar entre si, pára o programa"); // não pode haver ligação entre o mesmo nó
            return -1;
        } else if (!verificaExistenciaRamos(from1, too1, ramos)) {
            ramos[from1][too1] = Integer.parseInt(linhaRamos[2]); // linhaRamos[2] = 1 ---> ligação
            ramos[too1][from1] = Integer.parseInt(linhaRamos[2]);
            return 1;
        }
        String output = "Ligação " + from + " para " + too + " já existe, ignora linha";
        System.out.printf("%s%n", output);
        
        }
        else if (orientacao.equals("oriented")) {
            ramos[from1][too1] = Integer.parseInt(linhaRamos[2]);
            return 1;
        }
        return 0;
    }
    /**
     * A função verificaExistenciaRamos, recebe como parâmetro os índices de
     * origem e destino da ligação e a matriz de ramos. Caso já exista o ramo
     * retorna verdadeiro.
     *
     * @param from índices de origem
     * @param too destino da ligação
     * @param ramos matriz de ramos
     * @return boolean Caso já exista o ramo retorna verdadeiro.
     */
    private static boolean verificaExistenciaRamos(int from, int too, int[][] ramos) {
        if (ramos[too][from] == 1) { // se a ligação for de 1, esta já foi adicionada a matriz
            return true;
        }
        return false;
    }

    /**
     * A função grauNode é utilizada para calcular o grau de um node, que recebe
     * o node, a matriz e o número de entidades. O node é convertido num índice,
     * que serve para identificar a linha da matriz que é necessário percorrer.
     * Quando o elemento da matriz for igual a 1, é encontrada uma ligação e,
     * então o grau do node incrementa um valor.
     *
     * @param node guarda o node do qual queremos saber grau
     * @param matriz a nossa matriz preenchida
     * @param nEntidades número de nodes total
     * @return grau do node
     */
    static int grauNode(String node, int[][] matriz, int nEntidades) {

        int elemento = Integer.parseInt(String.valueOf(node.charAt(node.length() - 2)) + Integer.parseInt(String.valueOf(node.charAt(node.length() - 1)))); //transforma id entidade em inteiro
        int grau = 0;

        for (int i = 0; i < nEntidades; i++) {
            if (matriz[elemento - 1][i] == 1) {
                grau++;     // incrementa grau quando o elemento de uma linha é 1
            }

        }
        return grau;
    }

    /**
     * A função grauMedia é utilizada para calcular o grau médio de todos os
     * nodos. Recebe por parâmetro a matriz e o número de entidades, percorre
     * todos os nodos e calcula para cada um deles o seu grau. No fim retorna o
     * grau médio do nodo.
     *
     * @param matriz matriz a avaliar
     * @param nEntidades número de entidades
     * @return float grau médio do nodo
     */
    static float grauMedia(int[][] matriz, int nEntidades) {
        int grauTotal = 0;
        int grau = 0;
        //calcular o grau de cada um dos nodes
        for (int i = 0; i < nEntidades; i++) {
            grau += grauNode("s0" + (i + 1), matriz, nEntidades); // vai calcular o grau de cada linha e soma-os à medida que percorre a matriz

        }

        return (float) grau / nEntidades; // cast para float
    }

    /**
     * A função densidade calcula o quociente entre o número de ramos que esta
     * apresenta e o número máximo de ligações que poderia apresentar. A função
     * recebe a matriz e o número de entidades, calculando o máximo de ligações
     * que poderia apresentar se todos os nodos estivessem ligados entre si.
     * Posteriormente calcula-se o número de ramos da matriz, percorrendo a
     * matriz, incrementando uma variável de contagem sempre que encontrar o
     * valor 1, que simboliza um ramo. A função retorna o valor da variável.
     *
     * @param matriz a matriz a avaliar
     * @param nEntidades o número de entidades
     * @return a densidade da matriz
     */
    static float densidade(int[][] matriz, int nEntidades) {
        // densidade = m / M(máximo)
        int maximo = ((nEntidades * nEntidades) / 2) - (nEntidades / 2); //tiro aqueles que não podem existir (os que se ligam a si mesmo)
        int m = 0; // nosso m 
        for (int i = 1; i < nEntidades; i++) {
            for (int j = 0; j < i; j++) { // percorre apenas a parte inferior à diagonal dado que a matriz é simétrica
                if (matriz[i][j] == 1) {
                    m++;
                }

            }

        }
        float densidade = (float) m / maximo;
        return densidade;
    }

    /**
     * Método para calcular a centralidade do vetor próprio em que lhe passado
     * uma matriz e o número de entidades. A matriz inteira é convertida numa
     * matriz double. São utilizados os métodos da biblioteca (la4j) para fazer
     * a Eighendecomposition. São obtidos os valores próprio que servem de base
     * para obter o vetor próprio que é retornado.
     *
     * @param matriz a matriz a avaliar
     * @param nEntidades o número de entidades
     * @return um array, em que a primeira posição é o valor próprio e os
     * seguintes são o vector próprio
     */
    static double[] eigenvectorCentrality(int[][] matriz, int nEntidades) {
        double[] resultado = new double[nEntidades + 1];

        double[][] matDouble = new double[nEntidades][nEntidades];

        for (int i = 0; i < nEntidades; i++) {
            for (int j = 0; j < nEntidades; j++) {
                matDouble[i][j] = (double) matriz[i][j];
            }
        }

        Matrix a = new Basic2DMatrix(matDouble);

        //Obtem valores e vetores próprios fazendo "Eigen Decomposition"
        EigenDecompositor eigenD = new EigenDecompositor(a);

        Matrix[] mattD = eigenD.decompose();

        double matA[][] = mattD[0].toDenseMatrix().toArray();

        double matB[][] = mattD[1].toDenseMatrix().toArray();

        int indiceMaiorValor = -1;
        double maiorValor = Double.MIN_VALUE;

        for (int i = 0; i < matB.length; i++) {
            if (matB[i][i] > maiorValor) {
                maiorValor = matB[i][i];
                indiceMaiorValor = i;
            }
        }

        resultado[0] = (double) Math.round(maiorValor * 1000) / 1000;

        for (int i = 0; i < matA.length; i++) {
            resultado[i + 1] = (double) Math.round(matA[i][indiceMaiorValor] * 1000) / 1000;
        }

        return resultado;
    }

    /**
     * Este método retorna uma matriz que é o resultado da matriz elevada a k.
     * Este K é pedido ao utilizador.
     *
     * @param matriz
     * @param temp
     * @param nEntidades
     * @return
     */
    public static int[][] multiplicaMatriz(int[][] matriz, int[][] temp, int nEntidades) {
        int[][] result = new int[nEntidades][nEntidades]; // cria matriz auxiliar
        for (int i = 0; i < nEntidades; i++) {
            for (int j = 0; j < nEntidades; j++) {
                int somatorio = 0;
                for (int k = 0; k < nEntidades; k++) {
                    int produto = matriz[i][k] * temp[k][j]; // realiza operação de multiplicação de matrizes; percorre a linha da primeira e a coluna da segunda, simultaneamente 
                    somatorio += produto; //e multiplica os seus valor, adicionando a uma variavel somatorio que vai ser o elemento da matriz multiplicada
                }
                result[i][j] = somatorio; // preenche a matriz com o resultados das multiplicações
            }
        }
        return result;
    }

    /**
     * O programa tem a possibilidade de guardar os resultados escrevendo-os num
     * ficheiro, cujo nome é composto pelo nome da rede, seguida da data de
     * execução. O ficheiro de escrita tem de ser do tipo txt. O procedimento
     * FicheiroOutput, recebe por parâmetro o nome da rede, a contagem dos
     * elementos a inserir e um vector onde guarda a informação
     *
     * @param nomeRede o nome da rede de leitura
     * @param count a contagem dos elementos a inserir
     * @param vetorOutput vetor que guarda a informação que se pretende colocar
     * no ficheiro
     * @throws FileNotFoundException
     */
    public static void FicheiroOutput(String nomeRede, int count, String[] vetorOutput) throws FileNotFoundException {
        Date d = new Date();
        String dStr = DateFormat.getDateInstance(DateFormat.MEDIUM).format(d); // cria data ("dia/mes/ano")
        String[] data = dStr.split("/"); // remove barras
        String dataHoje = data[0] + data[1] + data[2]; // cria data sem barras
        String nameFile = (nomeRede + "_" + dataHoje + ".txt"); // cria no do ficheiro (ex: media_17dez2018.txt)
        Formatter fiche = new Formatter(new File(nameFile));

        fiche.format("%30s%n", nomeRede); // linha com nome da rede
        fiche.format("%s%n", "Cálculos efetuados no decorrer no uso da aplicação");
        fiche.format("%n");
        for (int i = 0; i < count; i++) {
            fiche.format("%s%n%n", vetorOutput[i]); // utiliza os outputs utilizados, ou seja, os calculos escolhidos pelo utilizador e adiciona-os ao ficheiro
        }
        fiche.close();
    }

    /**
     * O programa tem a possibilidade de calcular todas as métricas de uma só
     * vez, pelo que o procedimento executarTudo, realiza a leitura dos
     * ficheiros, cria a matriz, calcula todas as métricas e imprime para um
     * ficheiro. Recebe por parâmetro os nomes dos ficheiros onde estão
     * definidos os ramos e os nós, e ainda o valor inteiro da potência da
     * matriz que se pretende obter
     *
     * @param nomeFichNo nome do ficheiro de nós
     * @param nomeFichRamos nome do ficheiro de ramos
     * @param k o valor inteiro da potência da matriz que se pretende obter
     * @param entidades matriz com informação das entidades
     * @param vetorOutput vetor que contem os resultados das metricas
     * @param count numero de elementos do vetor de output
     * @param nEntidades numero de nos
     * @param ramos matriz que contem as ligações
     * @throws FileNotFoundException
     */
    private static void executarTudoNaoOrientado(String nomeFichNo, String nomeFichRamos, int k, String[][] entidades, String[] vetorOutput, int count, int nEntidades, int[][] ramos) throws FileNotFoundException {
        DecimalFormat df = new DecimalFormat("0.000"); // formatação para 3 casas decimais

        //faz o grau para cada node
        for (int i = 0; i < nEntidades; i++) {
            int grau = grauNode(entidades[i][0], ramos, nEntidades);
            String output = "Grau da entidade " + entidades[i][0] + " : " + grau;
            vetorOutput[count] = output;
            count++;
            System.out.println(output);
        }
        //faz o grau médio
        float grauMedio = grauMedia(ramos, nEntidades);
        String output = "Grau Medio: " + df.format(grauMedio);

        vetorOutput[count] = output;
        count++;
        System.out.println(output);

        //densidade da matriz
        float densidade = densidade(ramos, nEntidades);

        output = "Densidade: " + df.format(densidade);

        vetorOutput[count] = output;
        count++;
        System.out.println(output);

        System.out.println("Centralidade do Vector Próprio");

        double[] centrality = eigenvectorCentrality(ramos, nEntidades);

        output = "Valor Próprio: " + centrality[0];

        output = output + "\nCentralidade: ";

        for (int i = 1; i < centrality.length; i++) {
            output = output + centrality[i] + " ";
        }

        vetorOutput[count] = output;
        count++;
        System.out.println(output);

        String fi = "";
        int[][] aux = ramos;
        for (int i = 1; i < k; i++) { // k -> potencia 
            aux = multiplicaMatriz(ramos, aux, nEntidades);
            vetorOutput[count] = "Matriz Potência " + (i + 1);
            count++;
            System.out.println("");
            System.out.printf("%nMatriz Potência %d", (i + 1));
            System.out.println("");
            for (int z = 0; z < nEntidades; z++) {
                System.out.println("");
                fi = "";
                for (int j = 0; j < nEntidades; j++) {
                    System.out.printf("%d | ", aux[z][j]);
                    fi += aux[z][j] + " | ";
                }
                vetorOutput[count] = fi;
                count++;
            }
        }
        String[] nome = nomeFichNo.split(SEPARADOR_EXTENSAO_FICH); // obtem o nome da rede social, que se encontra no segundo elemento do vetor
        String[] label = nome[0].split(SEPARADOR_NOME_FICH);
        FicheiroOutput(label[1], count, vetorOutput);

    }
    /**
     * O programa tem a possibilidade de calcular todas as métricas de uma só
     * vez, pelo que o procedimento executarTudo, realiza a leitura dos
     * ficheiros, cria a matriz, calcula todas as métricas, imprime para um
     * ficheiro e abre a página com maior rank. Recebe por parâmetro os nomes dos ficheiros onde estão
     * definidos os ramos e os nós, o damping factor, o numero de iterações, a matriz de entidades, o vetor de outputs, o numero
     * de elementos do vetor de outputs, o numero de nos e a matriz de ligações
     *
     * @param nomeFichNo nome do ficheiro de nós
     * @param nomeFichRamos nome do ficheiro de ramos
     * @param dampingFactorP 
     * @param numeroIteracoes
     * @param entidades
     * @param vetorOutput
     * @param count
     * @param nEntidades
     * @param ramos
     * @throws FileNotFoundException
     */
    private static void executarTudoOrientado(String nomeFichNo, String nomeFichRamos, double dampingFactorP, int numeroIteracoes, String[][] entidades, String[] vetorOutput, int count, int nEntidades, int[][] ramos) throws FileNotFoundException {

        System.out.println("");
        for (int i = 0; i < nEntidades; i++) {
            int grauEntrada = LAPR1_PROJ_GrafosOrientados.calcularGrauEntrada(entidades[i][0], ramos, nEntidades);
            String output = "Grau de Entrada da entidade " + entidades[i][0] + " : " + grauEntrada;
            vetorOutput[count] = output;
            count++;
            System.out.println(output);
        }
        System.out.println("");

        for (int i = 0; i < nEntidades; i++) {
            int grauSaida = LAPR1_PROJ_GrafosOrientados.calcularGrauSaida(entidades[i][0], ramos, nEntidades);
            String output = "Grau de Saída da entidade " + entidades[i][0] + " : " + grauSaida;
            vetorOutput[count] = output;
            count++;
            System.out.println(output);
        }
        System.out.println("");

        try {
            if (dampingFactorP >= 0 && dampingFactorP <= 1) {

                int maiorPageRankPrint = -1;
                String output = "";
                if (numeroIteracoes > 0) {
                    System.out.println("PageRank pelo Algoritmo");
                    vetorOutput[count] = "PageRank pelo Algoritmo";
                    count++;

                    double[] pageRankVectorPage = LAPR1_PROJ_GrafosOrientados.calcularPageRank(ramos, nEntidades, numeroIteracoes, dampingFactorP);
                    System.out.println("");
                    pageRankVectorPage = LAPR1_PROJ_GrafosOrientados.vectorNormalization(pageRankVectorPage);
                    for (int i = 0; i < nEntidades; i++) {
                        output = output + pageRankVectorPage[i] + " ";
                    }
                    output = "Vetor Normalizado: " + output;
                    vetorOutput[count] = output;
                    count++;
                    System.out.println(output);

                    maiorPageRankPrint = LAPR1_PROJ_GrafosOrientados.maiorPageRank(pageRankVectorPage, nEntidades);

                    System.out.println("");
                    output = "Página com maior PageRank(por algoritmo) é: " + entidades[maiorPageRankPrint][0];

                    vetorOutput[count] = output;
                    count++;
                    System.out.println(output);
                    System.out.println("");
                    vetorOutput[count] = "PageRank pela Centralidade do Vetor Próprio";
                    count++;
                    System.out.println("PageRank pela Centralidade do Vetor Próprio");
                    double[] pageRankVector = LAPR1_PROJ_GrafosOrientados.calcularPageRankByCentrality(ramos, nEntidades, dampingFactorP);
                    pageRankVector = LAPR1_PROJ_GrafosOrientados.vectorNormalization(pageRankVector);
                    output = "";
                    for (int i = 0; i < nEntidades; i++) {
                        output = output + pageRankVector[i] + " ";
                    }
                    output = "Vetor Normalizador(Centralidade): " + output;
                    vetorOutput[count] = output;
                    count++;
                    System.out.println(output);
                    int maiorPageRank = LAPR1_PROJ_GrafosOrientados.maiorPageRank(pageRankVector, nEntidades);
                    output = "";
                    output = output + "Página com maior PageRank(por vetor próprio) é: " + entidades[maiorPageRank][0];

                    vetorOutput[count] = output;
                    count++;
                    System.out.println(output);
                    System.out.println("");
                    double distanciaEuclidiana = LAPR1_PROJ_GrafosOrientados.distanciaEuclidiana(pageRankVectorPage, pageRankVector);

                    String outputDist = "Distância euclidiana entre vectores PageRank calculados pelo Algoritmo com " + numeroIteracoes + " e pela Centralidade do Vetor Próprio: ";
                    outputDist = outputDist + distanciaEuclidiana;

                    vetorOutput[count] = outputDist;
                    count++;
                    System.out.println(outputDist);

                    Desktop desktop = java.awt.Desktop.getDesktop();
                    URI oURL = new URI(entidades[maiorPageRankPrint][4]);
                    desktop.browse(oURL);

                } else {
                    System.out.println("Valor Inválido!");

                }
            } else {
                System.out.println("Valor de Damping Factor Inválido!");

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        String[] nome = nomeFichNo.split(SEPARADOR_EXTENSAO_FICH); // obtem o nome da rede social, que se encontra no segundo elemento do vetor
        String[] label = nome[0].split(SEPARADOR_NOME_FICH);
        FicheiroOutput(label[1], count, vetorOutput);
    }
}
