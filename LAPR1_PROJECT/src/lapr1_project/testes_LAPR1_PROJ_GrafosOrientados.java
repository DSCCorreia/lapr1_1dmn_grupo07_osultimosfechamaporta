package lapr1_project;

import java.math.RoundingMode;
import java.text.DecimalFormat;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author OsÚltimosFechamAPorta Grupo07
 */
public class testes_LAPR1_PROJ_GrafosOrientados {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int[][] matrizTeste1 = {{0, 0, 1, 1}, {1, 0, 0, 0}, {1, 0, 0, 1}, {1, 1, 1, 0}};
        int[][] matrizTeste2 = {{0, 0, 1, 0}, {1, 0, 0, 0}, {1, 0, 0, 0}, {1, 1, 1, 0}};

        double[][] matrizTesteA = {{0, 0, 0.5, 0, 0, 0, 0, 0},
        {0.333, 0, 0, 0.5, 0, 0, 0, 0},
        {0.333, 0, 0, 0, 0, 0, 0, 0, 0},
        {0.333, 0.5, 0.5, 0, 0, 0, 0, 0},
        {0, 0.5, 0, 0, 0, 0.5, 0, 0},
        {0, 0, 0, 0, 0, 0, 1, 0.5},
        {0, 0, 0, 0.5, 1, 0, 0, 0.5},
        {0, 0, 0, 0, 0, 0.5, 0, 0}};

        int[][] matrizPageRank = {{0, 0, 1, 0, 0, 0, 0, 0},
        {1, 0, 0, 1, 0, 0, 0, 0},
        {1, 0, 0, 0, 0, 0, 0, 0, 0},
        {1, 1, 1, 0, 0, 0, 0, 0},
        {0, 1, 0, 0, 0, 1, 0, 0},
        {0, 0, 0, 0, 0, 0, 1, 1},
        {0, 0, 0, 1, 1, 0, 0, 1},
        {0, 0, 0, 0, 0, 1, 0, 0}};

        double[][] expectedMatrixA = {{0, 0, 0.5, 0.5}, {0.333, 0, 0, 0}, {0.333, 0, 0, 0.5}, {0.333, 1, 0.5, 0}};

        double[][] expectedMatrixA2 = {{0, 0, 0.5, 0.25}, {0.333, 0, 0, 0.25}, {0.333, 0, 0, 0.25}, {0.333, 1, 0.5, 0.25}};

        double[][] expectedMatrixM = {{0.0187, 0.0187, 0.4437, 0.0187, 0.0187, 0.0187, 0.0187, 0.0187},
        {0.3021, 0.0187, 0.0187, 0.4437, 0.0187, 0.0187, 0.0187, 0.0187},
        {0.3021, 0.0187, 0.0187, 0.0187, 0.0187, 0.0187, 0.0187, 0.0187},
        {0.3021, 0.4437, 0.4437, 0.0187, 0.0187, 0.0187, 0.0187, 0.0187},
        {0.0187, 0.4437, 0.0187, 0.0187, 0.0187, 0.4437, 0.0187, 0.0187},
        {0.0187, 0.0187, 0.0187, 0.0187, 0.0187, 0.0187, 0.8688, 0.4437},
        {0.0187, 0.0187, 0.0187, 0.4437, 0.8688, 0.0187, 0.0187, 0.4437},
        {0.0187, 0.0187, 0.0187, 0.0187, 0.0187, 0.4437, 0.0187, 0.0187}};

        double[] expectedVetorRank = {0.0304, 0.0543, 0.0274, 0.0623, 0.1615, 0.2827, 0.2392, 0.1382};
        double[][] matrizTeste3 = {{0.1, 0.2, 0.1}, {0.3, 0.1, 0.2}, {0.4, 0.1, 0.3}};
        double[] vetorTeste1 = {0.2, 0.1, 0.2};
        double[] expectedVetor1 = {0.06, 0.11, 0.15};

        double[] vectorTeste2 = {3, 1, 2};
        double[] expectedVetorNormalizated = {0.802, 0.267, 0.535};

        double[] vectorEuc1 = {0, 3, 4, 5};
        double[] vectorEuc2 = {7, 6, 3, -1};
        double expectedDistEucl = 9.747;

        if (testGrauEntrada(matrizTeste1, "s01", 4, 2) && testGrauEntrada(matrizTeste1, "s02", 4, 1) && testGrauEntrada(matrizTeste1, "s03", 4, 2) && testGrauEntrada(matrizTeste1, "s04", 4, 3)) {
            System.out.println("\nTeste Grau Entrada : PASS");
        } else {
            System.out.println("\nTeste Grau Entrada : FAIL");
        }

        if (testGrauSaida(matrizTeste1, "s01", 4, 3) && testGrauSaida(matrizTeste1, "s02", 4, 1) && testGrauSaida(matrizTeste1, "s03", 4, 2) && testGrauSaida(matrizTeste1, "s04", 4, 2)) {
            System.out.println("\nTeste Grau Saída : PASS");
        } else {
            System.out.println("\nTeste Grau Saída : FAIL");
        }

        if (testGrauSaidaByIndex(matrizTeste2, 0, 4, 3) && testGrauSaidaByIndex(matrizTeste2, 1, 4, 1) && testGrauSaidaByIndex(matrizTeste2, 2, 4, 2) && testGrauSaidaByIndex(matrizTeste2, 3, 4, 0)) {
            System.out.println("\nTeste Grau Saída By Index : PASS");
        } else {
            System.out.println("\nTeste Grau Saída By Index : FAIL");
        }

        if (testPreencheMatrixA(matrizTeste1, 4, expectedMatrixA)) {
            System.out.println("\nTeste Preenche Matriz : PASS");
        } else {
            System.out.println("\nTeste Preenche Matriz : FAIL");
        }

        if (testPreencheMatrixA(matrizTeste2, 4, expectedMatrixA2)) {
            System.out.println("\nTeste Preenche Matriz (dangling node) : PASS");
        } else {
            System.out.println("\nTeste Preenche Matriz (dangling node) : FAIL");
        }

        if (testConverterMatrixM(matrizTesteA, 8, expectedMatrixM, 0.85)) {
            System.out.println("\nTeste Converter Matrix M : PASS");
        } else {
            System.out.println("\nTeste Converter Matrix M : FAIL");
        }

        if (testMultiplicarMatriz(matrizTeste3, vetorTeste1, expectedVetor1)) {
            System.out.println("\nTeste de multiplicar matriz por vetor: PASS");
        } else {
            System.out.println("\nTeste de multiplicar matriz por vetor: FAIL");
        }

        if (testCalcularPageRank(matrizPageRank, 8, 8, expectedVetorRank, 0.85)) {
            System.out.println("\nTeste de calcular Page Rank: PASS");
        } else {
            System.out.println("\nTeste de calcular Page Rank: FAIL");
        }

        if (testNormalizarVector(vectorTeste2, expectedVetorNormalizated)) {
            System.out.println("\nTeste de normalizar vector: PASS");
        } else {
            System.out.println("\nTeste de normalizar vector: FAIL");
        }

        if (testDistanciaEuclidiana(vectorEuc1, vectorEuc2, expectedDistEucl)) {
            System.out.println("\nTeste de distancia Euclidiana entre vectores: PASS");
        } else {
            System.out.println("\nTeste de distancia Euclidiana entre vectores: FAIL");
        }
    }

    /**
     * Teste para avaliar o método de multiplicar matriz com vector. Recebe por
     * parâmetros a matriz e o vector e o resultado esperado.
     *
     * @param matriz a matriz a multiplicar
     * @param vetorTeste1 o vector a multiplicar
     * @param result o vector resultado esperado
     * @return true se o teste passa, false se o teste falha.
     */
    public static boolean testMultiplicarMatriz(double[][] matriz, double[] vetorTeste1, double[] result) {
        double[] r = LAPR1_PROJ_GrafosOrientados.multiplicarMatrizPorVetor(matriz, vetorTeste1);

        return compararVector(result, r);

    }

    /**
     * Teste para avaliar resultado retornado pelo grau de Entrada. A função
     * recebe a matriz, o nó, o número de entidades e o resultado esperado.
     * Calcula o grau de entrada do nó na matriz de n entidades e compara com o
     * resultado esperado
     *
     * @param matriz a matriz onde se encontra o nó em estudo
     * @param node o nó em estudo para o grau de entrada
     * @param nEntidades o número de entidades na matriz
     * @param expectedResult o resultado esperado
     * @return true se o teste passa, se falha retorna false.
     */
    public static boolean testGrauEntrada(int[][] matriz, String node, int nEntidades, int expectedResult) {

        int result = LAPR1_PROJ_GrafosOrientados.calcularGrauEntrada(node, matriz, nEntidades);

        if (result == expectedResult) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Teste para avaliar resultado retornado pelo grau de Saída. A função
     * recebe a matriz, o nó, o número de entidades e o resultado esperado.
     * Calcula o grau de saída do nó na matriz de n entidades e compara com o
     * resultado esperado
     *
     * @param matriz a matriz onde se encontra o nó em estudo
     * @param node o nó em estudo para o grau de saída
     * @param nEntidades o número de entidades na matriz
     * @param expectedResult o resultado esperado
     * @return true se o teste passa, se falha retorna false.
     */
    public static boolean testGrauSaida(int[][] matriz, String node, int nEntidades, int expectedResult) {

        int result = LAPR1_PROJ_GrafosOrientados.calcularGrauSaida(node, matriz, nEntidades);

        if (result == expectedResult) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Teste para avaliar resultado retornado pelo grau de Saída By Index. A
     * função recebe a matriz, o indice do nó, o número de entidades e o
     * resultado esperado. Calcula o grau de saída do nó na matriz de n
     * entidades e compara com o resultado esperado
     *
     * @param matriz a matriz onde se encontra o nó em estudo
     * @param index o indice do nó em estudo para o grau de saída
     * @param nEntidades o número de entidades na matriz
     * @param expectedResult o resultado esperado
     * @return true se o teste passa, se falha retorna false.
     */
    public static boolean testGrauSaidaByIndex(int[][] matriz, int index, int nEntidades, int expectedResult) {

        int result = LAPR1_PROJ_GrafosOrientados.calcularGrauSaidaByIndex(index, matriz, nEntidades);

        if (result == expectedResult) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Teste para avaliar resultado retornado pelo preenche Matriz A. A função
     * recebe a matriz, o número de entidades e o resultado esperado. Preenche a
     * Matriz A de n entidades e compara com o resultado esperado
     *
     *
     * @param matriz a matriz em estudo
     * @param nEntidades o numero de entidades na matriz
     * @param expectedResult o resultado esperado
     * @return true se teste passa, false se falha
     */
    public static boolean testPreencheMatrixA(int[][] matriz, int nEntidades, double[][] expectedResult) {

        double[][] result = LAPR1_PROJ_GrafosOrientados.preencherMatrixA(matriz, nEntidades);

        if (compararMatriz(result, expectedResult)) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Teste para avaliar resultado retornado pelo métodoCalcularPageRank. A
     * função recebe a matriz, o número de entidades, o número de iterações, o
     * resultado esperado e o damping factor. Preenche o vector PageRank de n
     * entidades e compara com o resultado esperado
     *
     * @param matriz a matriz em estudo
     * @param nEntidades o número de entidades da rede
     * @param nIteracoes o número de iterações que se pretendem realizar
     * @param expectedResult o resultado esperado
     * @param factor damping factor
     *
     * @return true se o teste passa, false se falha
     */
    public static boolean testCalcularPageRank(int[][] matriz, int nEntidades, int nIteracoes, double[] expectedResult, double factor) {

        double[] result = LAPR1_PROJ_GrafosOrientados.calcularPageRank(matriz, nEntidades, nIteracoes, factor);

        if (compararVector(result, expectedResult)) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * /**
     * Teste para avaliar resultado retornado pelo converte Matriz M. A função
     * recebe a matriz A, o número de entidades, o resultado esperado e o
     * damping factor. Preenche a Matriz M de n entidades e compara com o
     * resultado esperado
     *
     *
     * @param matriz a matriz em estudo
     * @param nEntidades o numero de entidades na matriz
     * @param expectedResult o resultado esperado
     * @param factor damping factor
     *
     * @return true se teste passa, false se falha
     */
    public static boolean testConverterMatrixM(double[][] matriz, int nEntidades, double[][] expectedResult, double factor) {

        double[][] result = LAPR1_PROJ_GrafosOrientados.converterParaMatrizM(matriz, nEntidades, factor);

        if (compararMatrizM(result, expectedResult)) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Teste para avaliar o resultado retornado pela função vectorNormalization.
     * A função recebe o vector a normalizar e o resultado esperado. Normaliza o
     * vector recebido e compara com o resultado esperado.
     *
     * @param vector o vector a normalizar
     * @param expectedResult o resultado esperado
     *
     * @return true se passa no teste, false se falha
     */
    public static boolean testNormalizarVector(double[] vector, double[] expectedResult) {

        double[] result = LAPR1_PROJ_GrafosOrientados.vectorNormalization(vector);

        if (compararVector(result, expectedResult)) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Teste para avaliar o resultado retornado pela função distanciaEuclidiana.
     * A função recebe dois vectores e o resultado esperado. Calcula a distância
     * euclidiana entre os dois vectores e compara com o resultado esperado.
     *
     * @param vectorEuc1 1º vector
     * @param vectorEuc2 2º vector
     * @param expectedDistEucl o resultado esperado
     *
     * @return true se o teste passa, false se falha
     */
    public static boolean testDistanciaEuclidiana(double[] vectorEuc1, double[] vectorEuc2, double expectedDistEucl) {

        double result = LAPR1_PROJ_GrafosOrientados.distanciaEuclidiana(vectorEuc1, vectorEuc2);

        result = Math.round(result * 1000);
        result = result / 1000;
        expectedDistEucl = Math.round(expectedDistEucl * 1000);
        expectedDistEucl = expectedDistEucl / 1000;

        if (result == expectedDistEucl) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Método para Calcular duas matrizes de double
     *
     * @param result a 1ª matriz
     * @param expectedResult a 2ª matriz
     * @return true se iguais, false se diferentes
     */
    private static boolean compararMatriz(double[][] result, double[][] expectedResult) {

        if (result.length == expectedResult.length) {
            for (int i = 0; i < result.length; i++) {
                if (result[i].length == expectedResult[i].length) {
                    for (int j = 0; j < result[i].length; j++) {
                        result[i][j] = Math.round(result[i][j] * 1000);
                        result[i][j] = result[i][j] / 1000;
                        if (result[i][j] != expectedResult[i][j]) {
                            return false;
                        }
                    }
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
        return true;

    }

    /**
     * Compara duas Matrizes do tipo M
     *
     * @param result Matriz M em estudo
     * @param expectedResult Matriz esperada
     * @return true se iguais, false se diferentes
     */
    private static boolean compararMatrizM(double[][] result, double[][] expectedResult) {

        if (result.length == expectedResult.length) {
            for (int i = 0; i < result.length; i++) {
                if (result[i].length == expectedResult[i].length) {
                    for (int j = 0; j < result[i].length; j++) {
                        result[i][j] = Math.round(result[i][j] * 1000);
                        result[i][j] = result[i][j] / 1000;
                        expectedResult[i][j] = Math.round(expectedResult[i][j] * 1000);
                        expectedResult[i][j] = expectedResult[i][j] / 1000;

                        if (result[i][j] != expectedResult[i][j]) {
                            return false;
                        }
                    }
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
        return true;

    }

    /**
     * Método para comparar vectores. Recebe os vectores result e
     * expectedResult, comparando-os e retorna um boolean.
     *
     * @param result o 1º vector a comparar
     * @param expectedResult o 2º vector a comparar
     *
     * @return true se iguais, false se diferentes.
     */
    private static boolean compararVector(double[] result, double[] expectedResult) {

        if (result.length == expectedResult.length) {
            for (int i = 0; i < result.length; i++) {

                result[i] = Math.round(result[i] * 100);
                result[i] = result[i] / 100;
                expectedResult[i] = Math.round(expectedResult[i] * 100);
                expectedResult[i] = expectedResult[i] / 100;
                if (result[i] != expectedResult[i]) {
                    return false;
                }
            }
        } else {
            return false;
        }

        return true;
    }

}
