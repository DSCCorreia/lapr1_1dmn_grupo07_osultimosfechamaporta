/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr1_project;

import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.util.Arrays;

/**
 *
 * @author OsÚltimosFechamAPorta Grupo07
 */
public class testes {

    public static void main(String[] args) {
        int[][] matriz = {{0, 1, 1}, {1, 0, 1}, {1, 1, 0}};
        int[][] matriz2 = {{0, 1, 0}, {1, 0, 1}, {0, 1, 0}};
        double[] expResult2 = {0.500, 0.707, 0.500};
        int[][] matriz3 = {{0, 1, 0, 1, 0}, {1, 0, 1, 1, 1}, {0, 1, 0, 1, 0}, {1, 1, 1, 0, 0}, {0, 1, 0, 0, 0}};
        
        double[] expResult3 = {0.412, 0.583, 0.412, 0.524, 0.217};

        if (testGrauNode(matriz, "s01", 3)) {
            System.out.println("True");
        }
        if (testGrauMedia(matriz, 3)) {
            System.out.println("True");
        }
        if (testDensidade(matriz, 3)) {
            System.out.println("True");
        }
        if (testMultiplicação(matriz, 3)) {
            System.out.println("True");
        }
        if (testEigenvectorCentrality(matriz2, 3, expResult2)) {
            System.out.println("True");
        }

        if (testEigenvectorCentrality(matriz3, 5, expResult3)) {
            System.out.println("True");
        }

    }

    public static boolean testGrauNode(int[][] matriz, String node, int nEntidades) {

        int expectedGrau = 2;
        int result = LAPR1_PROJECT_MAIN.grauNode(node, matriz, nEntidades);

        if (result == expectedGrau) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean testGrauMedia(int[][] matriz, int nEntidades) {
        float expectedGrauMedia = 2f;
        float result = LAPR1_PROJECT_MAIN.grauMedia(matriz, nEntidades);

        if (result == expectedGrauMedia) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean testDensidade(int[][] matriz, int nEntidades) {
        float expectedDensidade = 1f;

        float result = LAPR1_PROJECT_MAIN.densidade(matriz, nEntidades);
        if (result == expectedDensidade) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean testMultiplicação(int[][] matriz, int nEntidades) {
        int[][] comp = {{2, 1, 1}, {1, 2, 1}, {1, 1, 2}};

        matriz = LAPR1_PROJECT_MAIN.multiplicaMatriz(matriz, matriz, nEntidades);
        for (int i = 0; i < nEntidades; i++) {
            for (int j = 0; j < nEntidades; j++) {
                if (matriz[i][j] != comp[i][j]) {
                    return false;
                }
            }
        }

        return true;

    }

    public static boolean testEigenvectorCentrality(int[][] matrix, int nEntidades, double[] expResult) {

        double[] result = LAPR1_PROJECT_MAIN.eigenvectorCentrality(matrix, nEntidades);

        if (Arrays.equals(result, expResult)) {
            return true;
        }
        return false;

    }
}
