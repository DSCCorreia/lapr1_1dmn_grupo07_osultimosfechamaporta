/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr1_project;

import org.la4j.Matrix;
import org.la4j.decomposition.EigenDecompositor;
import org.la4j.matrix.dense.Basic2DMatrix;

/**
 *
 * @author OsÚltimosFechamAPorta Grupo07
 */
public class LAPR1_PROJ_GrafosOrientados {

    /**
     * Método para calcular o grau de entrada de um nó. O grau de entrada de um
     * nó é o número de ligações estabelecidas entre dois nós e que têm como
     * destino o nó avaliado.
     *
     * A função calcularGrauEntrada recebe o nó, a matriz e o número de
     * entidades existentes na matriz.
     *
     * O node é convertido num índice, que serve para identificar a linha da
     * matriz que é necessário percorrer. Quando o elemento da matriz for igual
     * a 1, é encontrada uma ligação com destino no nó em estudo, então o grau
     * de entrada incrementa um valor.
     *
     * @param node guarda o nó que se pretende calcular o grau de entrada
     * @param matriz nossa matriz preenchida
     * @param nEntidades número total de nós
     * @return grau de entrada do nó
     */
    public static int calcularGrauEntrada(String node, int[][] matriz, int nEntidades) {

        int elemento = Integer.parseInt(String.valueOf(node.charAt(node.length() - 2)) + Integer.parseInt(String.valueOf(node.charAt(node.length() - 1)))); //transforma id entidade em inteiro
        int grauEntrada = 0;

        for (int i = 0; i < nEntidades; i++) {
            if (matriz[elemento - 1][i] == 1) {
                grauEntrada++;     // incrementa grau quando o elemento de uma linha é 1
            }

        }

        return grauEntrada;
    }

    /**
     * Método para calcular o grau de saída de um nó. O grau de saída de um nó é
     * o número de ligações estabelecidas entre dois nós e que têm como origem o
     * nó avaliado.
     *
     * A função calcularGrauSaida recebe o nó, a matriz e o número de entidades
     * existentes na matriz.
     *
     * O node é convertido num índice, que serve para identificar a coluna da
     * matriz que é necessário percorrer. Quando o elemento da matriz for igual
     * a 1, é encontrada uma ligação com origem no nó em estudo, então o grau de
     * saída incrementa um valor.
     *
     * @param node guarda o nó que se pretende calcular o grau de saída
     * @param matriz nossa matriz preenchida
     * @param nEntidades número total de nós
     * @return grau de saída do nó
     */
    public static int calcularGrauSaida(String node, int[][] matriz, int nEntidades) {

        int elemento = Integer.parseInt(String.valueOf(node.charAt(node.length() - 2)) + Integer.parseInt(String.valueOf(node.charAt(node.length() - 1)))); //transforma id entidade em inteiro
        int grauSaida = 0;

        for (int i = 0; i < nEntidades; i++) {
            if (matriz[i][elemento - 1] == 1) {
                grauSaida++;     // incrementa grau quando o elemento de uma linha é 1
            }

        }

        return grauSaida;
    }

    /**
     * Método para calcular o grau de saída de um nó. O grau de saída de um nó é
     * o número de ligações estabelecidas entre dois nós e que têm como origem o
     * nó avaliado.
     *
     * A função calcularGrauSaidaByIndex recebe o indice do nó, a matriz e o
     * número de entidades existentes na matriz.
     *
     * O indice serve para identificar a coluna da matriz que é necessário
     * percorrer. Quando o elemento da matriz for igual a 1, é encontrada uma
     * ligação com origem no nó em estudo, então o grau de saída incrementa um
     * valor.
     *
     * @param index guarda o nó que se pretende calcular o grau de saída
     * @param matriz nossa matriz preenchida
     * @param nEntidades número total de nós
     * @return grau de saída do nó
     */
    public static int calcularGrauSaidaByIndex(int index, int[][] matriz, int nEntidades) {

        int grauSaida = 0;

        for (int i = 0; i < nEntidades; i++) {
            if (matriz[i][index] == 1) {
                grauSaida++;     // incrementa grau quando o elemento de uma linha é 1
            }

        }

        return grauSaida;
    }

    /**
     * Método para calcular o vector PageRank de uma Matriz. A função recebe a
     * matriz da rede em estudo, o número de entidades, o número de iterações em
     * page rank e o damping factor . Retorna o vector Page Rank.
     *
     * @param matriz a matriz em estudo
     * @param nEntidades o número de entidades na rede
     * @param nIteracoes o número de iterações em pageRank
     * @param factor o factor damping
     *
     * @return o vector pageRank
     */
    public static double[] calcularPageRank(int[][] matriz, int nEntidades, int nIteracoes, double factor) {

        double[][] matrizA = preencherMatrixA(matriz, nEntidades);

        double[][] matrizM = converterParaMatrizM(matrizA, nEntidades, factor);

        double[] vecCalc = new double[nEntidades];

        for (int i = 0; i < nEntidades; i++) {
            vecCalc[i] = 1 / (double) nEntidades;
        }

        for (int j = 0; j < vecCalc.length; j++) {
            if (j == 0) {
                System.out.print("\n" + vecCalc[j]);
            } else {
                System.out.print(" " + vecCalc[j]);
            }
        }

        for (int i = 0; i < nIteracoes; i++) {
            vecCalc = multiplicarMatrizPorVetor(matrizM, vecCalc);
            for (int j = 0; j < vecCalc.length; j++) {
                if (j == 0) {
                    System.out.print("\n" + vecCalc[j]);
                } else {
                    System.out.print(" " + vecCalc[j]);
                }
            }
        }

        return vecCalc;
    }

    /**
     * Método para retornar o indíce da página que apresenta maior PageRank.
     * Recebe o vector de pageRank e o número de entidades e retorna o indíce da
     * página com maior pageRank.
     *
     * @param vector vector de pageRank
     * @param nEntidades número de entidades na rede
     * @return o indice com maior pageRank
     */
    public static int maiorPageRank(double[] vector, int nEntidades) {
        int indexMaior = 0;
        double valorMaior = Double.MIN_VALUE;
        for (int i = 0; i < nEntidades; i++) {
            if (vector[i] > valorMaior) {
                valorMaior = vector[i];
                indexMaior = i;
            }
        }

        return indexMaior;
    }

    /**
     * Método para preencher a Matriz A, o indice a(i,j) é igual a 1/n se j é um
     * dandling node, é 1/(grau de saida de j) se há ligação de j para i, ou é 0
     * nos restantes casos.
     *
     * A função recebe a matriz para avaliar as ligações e o número de entidades
     * na matriz. Retorna uma Matriz A, devidamente preenchida.
     *
     * @param matriz matriz para avaliar as ligações
     * @param nEntidades o número de entidades presentes na matriz
     * @return Matriz A, preenchida.
     */
    public static double[][] preencherMatrixA(int[][] matriz, int nEntidades) {

        double[][] matA = new double[nEntidades][nEntidades];
        int[] grauSaida = new int[nEntidades];

        for (int i = 0; i < nEntidades; i++) {
            grauSaida[i] = calcularGrauSaidaByIndex(i, matriz, nEntidades);
        }

        for (int i = 0; i < matA.length; i++) {
            for (int j = 0; j < matA[i].length; j++) {
                matA[i][j] = 0;
                if (grauSaida[j] == 0) {
                    double num = 1 / (double) nEntidades;

                    matA[i][j] = num;

                } else if (matriz[i][j] == 1) {
                    double num = 1 / (double) grauSaida[j];
                    matA[i][j] = num;

                }

            }
        }

        return matA;
    }

    /**
     * Método para converter a matriz A em matriz M, recebe como parâmetro a
     * matriz A, o número de entidades e o damping factor. Retorna a matriz M.
     *
     * @param matriz matriz A a converter
     * @param nEntidades número de entidades na rede
     * @param factor damping factor
     *
     * @return a matriz M
     */
    public static double[][] converterParaMatrizM(double[][] matriz, int nEntidades, double factor) {

        double[][] matM = new double[nEntidades][nEntidades];

        for (int i = 0; i < nEntidades; i++) {
            for (int j = 0; j < nEntidades; j++) {
                matM[i][j] = factor * matriz[i][j] + ((1 - factor) / (double) nEntidades);
                matM[i][j] = Math.round(matM[i][j] * 10000);
                matM[i][j] = matM[i][j] / 10000;
            }
        }

        return matM;
    }

    /**
     * Método para calcularmos. Este é o resultado da Matriz
     *
     * @param matriz
     * @param vetor
     * @return
     */
    public static double[] multiplicarMatrizPorVetor(double[][] matriz, double[] vetor) {
        double[] resultado = new double[matriz.length];
        double[][] aux = new double[matriz.length][matriz.length];

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                aux[i][j] = matriz[i][j] * vetor[j];
                aux[i][j] = Math.round(aux[i][j] * 1000);
                aux[i][j] = aux[i][j] / 1000;
            }
        }
        for (int i = 0; i < aux.length; i++) {
            for (int j = 0; j < aux[0].length; j++) {
                resultado[i] += aux[i][j];
            }
            resultado[i] = Math.round(resultado[i] * 1000);
            resultado[i] = resultado[i] / 1000;
        }
        return resultado;
    }

    /**
     * Método para normalizar o vector. A função recebe um vector e retorna um
     * segundo vector, resultado da normalização do primeiro.
     *
     * @param vector o vector a normalizar
     *
     * @return o vector normalizado
     */
    public static double[] vectorNormalization(double[] vector) {

        double[] normalizatedVector = new double[vector.length];

        double vectorNorm = 0;

        for (int i = 0; i < vector.length; i++) {
            vectorNorm = vectorNorm + (vector[i] * vector[i]);
        }

        vectorNorm = Math.sqrt(vectorNorm);

        for (int i = 0; i < vector.length; i++) {
            normalizatedVector[i] = vector[i] / vectorNorm;
            normalizatedVector[i] = (double) Math.round(normalizatedVector[i] * 1000) / 1000;
        }

        return normalizatedVector;
    }

    /**
     * Método para calcular o Page Rank através do vector Próprio da Matriz M. A
     * função recebe a matriz da rede a analisar, o número de entidades da rede
     * e o damping factor. Converte em matriz M e calcula o seu vector próprio,
     * que corresponde à PageRank pela centralidade do vector próprio.
     *
     * @param matriz a matriz da rede em estudo.
     * @param nEntidades o número de entidades da rede.
     * @param factor o damping factor
     *
     * @return o vector próprio da matriz
     */
    public static double[] calcularPageRankByCentrality(int[][] matriz, int nEntidades, double factor) {

        double[][] matrizA = preencherMatrixA(matriz, nEntidades);

        double[][] matrizM = converterParaMatrizM(matrizA, nEntidades, factor);

        double[] resultado = new double[nEntidades];

        Matrix a = new Basic2DMatrix(matrizM);

        //Obtem valores e vetores próprios fazendo "Eigen Decomposition"
        EigenDecompositor eigenD = new EigenDecompositor(a);

        Matrix[] mattD = eigenD.decompose();

        double matA[][] = mattD[0].toDenseMatrix().toArray();

        double matB[][] = mattD[1].toDenseMatrix().toArray();

        int indiceMaiorValor = -1;
        double maiorValor = Double.MIN_VALUE;

        for (int i = 0; i < matB.length; i++) {
            if (matB[i][i] > maiorValor) {
                maiorValor = matB[i][i];
                indiceMaiorValor = i;
            }
        }

        for (int i = 0; i < matA.length; i++) {
            if (resultado[i] > 0) {
                resultado[i] = (double) Math.round(matA[i][indiceMaiorValor] * 1000) / 1000;
            } else {
                resultado[i] = ((double) Math.round(matA[i][indiceMaiorValor] * 1000) / 1000) * -1;
            }

        }

        return resultado;
    }

    /**
     * Método para calcular a distancia Euclidiana entre dois vectores. A função
     * recebe dois vectores, calcula a distância Euclidiana entre os dois
     * vectores e retorna esse valor.
     *
     * @param vector1 1º vector
     * @param vector2 2º vector
     * @return a distanciaEuclidiana entre os dois vectores.
     */
    public static double distanciaEuclidiana(double[] vector1, double[] vector2) {

        double distanciaEuclidiana = 0;

        for (int i = 0; i < vector1.length; i++) {
            double temp = vector1[i] - vector2[i];
            distanciaEuclidiana = distanciaEuclidiana + (temp * temp);
        }

        distanciaEuclidiana = Math.sqrt(distanciaEuclidiana);

        return distanciaEuclidiana;
    }

}
